package com.qwerty.practice.repository;

import com.qwerty.practice.entity.Reviews;
import com.qwerty.practice.entity.Watchlist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WatchlistRepo  extends JpaRepository<Watchlist, Long> {
    List<Watchlist> findByUserId(Long userId);
    void deleteByMovieIdAndUserId(Long movieId, Long userId);
    Watchlist findByMovieIdAndUserId(Long movieId, Long userId);
}
