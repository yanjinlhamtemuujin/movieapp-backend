package com.qwerty.practice.repository;
import com.qwerty.practice.entity.Reviews;
import com.qwerty.practice.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReviewRepo extends JpaRepository<Reviews, Long> {
    List<Reviews> findAllBy();

    @Query("select new com.qwerty.practice.entity.Reviews(a, b.userName) " +
            "from Reviews a " +
            "left join User b on a.userId = b.userId " +
            "where a.movieId = ?1 " +
            "order by a.date desc")
    List<Reviews> findByMovieId(Long movieId);
}
