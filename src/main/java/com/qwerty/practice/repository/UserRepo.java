package com.qwerty.practice.repository;
import com.qwerty.practice.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {
    List<User> findAllBy();
    List<User> findByUserName(String userName);
    User findByUserNameAndPassword(String userName, String password);
    User findByEmail(String email);

}



