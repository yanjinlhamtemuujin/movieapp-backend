package com.qwerty.practice.repository;

import com.qwerty.practice.entity.Persons;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepo extends JpaRepository<Persons, Long> {
    List<Persons> findAllBy();
}
