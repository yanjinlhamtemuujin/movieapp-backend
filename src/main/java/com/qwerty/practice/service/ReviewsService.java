package com.qwerty.practice.service;

import com.qwerty.practice.entity.Reviews;
import com.qwerty.practice.repository.ReviewRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Optional;


@Service
@CrossOrigin(origins = "http://localhost:4200", allowCredentials = "true")
public class ReviewsService {

    @Autowired
    ReviewRepo reviewRepo;

    public Object getAllReviews() {
        return reviewRepo.findAllBy();
    }

    public List<Reviews> getOneReviews(Long id) {
        return reviewRepo.findByMovieId(id);
    }

    public Object addReview(Reviews reviews) { return reviewRepo.save(reviews); }

    public void deleteReviews(Long id) {
        reviewRepo.deleteById(id);
    }
}
