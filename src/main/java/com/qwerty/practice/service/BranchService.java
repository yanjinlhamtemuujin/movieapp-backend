package com.qwerty.practice.service;

import com.qwerty.practice.entity.Branch;
import com.qwerty.practice.exception.BranchNotFoundException;
import com.qwerty.practice.repository.BranchRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BranchService {

    @Autowired
    BranchRepo branchRepo;

    public Object getAllBranch() {
        return branchRepo.findAllBy();
    }

    public Object getOneBranch(Long id) {
        return branchRepo.findById(id).orElseThrow(() -> new BranchNotFoundException(id));
    }

    public Object addBranch(Branch branch) {
        return branchRepo.save(branch);
    }

    public Object replaceBranch(Branch newBranch, Long id) {
        return branchRepo.findById(id)
                .map(branch -> {
                    branch.setBranchName(newBranch.getBranchName());
                    branch.setMgrId(newBranch.getMgrId());
                    return branchRepo.save(branch);
                })
                .orElseGet(() -> {
                    newBranch.setBranchId(id);
                    return branchRepo.save(newBranch);
                });
    }

    public void deleteBranch(Long id) {
        branchRepo.deleteById(id);
    }
}
