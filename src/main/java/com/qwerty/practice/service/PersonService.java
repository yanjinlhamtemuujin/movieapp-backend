package com.qwerty.practice.service;

import com.qwerty.practice.entity.Persons;
import com.qwerty.practice.entity.Branch;
import com.qwerty.practice.exception.PersonNotFoundException;
import com.qwerty.practice.repository.PersonRepo;
import com.qwerty.practice.repository.BranchRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonService {

    @Autowired
    PersonRepo personRepo;
    BranchRepo branchRepo;

    public Object getAllPerson() {
        return personRepo.findAllBy();
    }

    public Object getOnePerson(Long id) {
        return personRepo.findById(id).orElseThrow(() -> new PersonNotFoundException(id));
    }

    public Object addPerson(Persons persons) {
        return personRepo.save(persons);
    }

    public Object replacePerson(Persons newPerson, Long id) {
        return personRepo.findById(id)
                .map(person -> {
                    person.setLastName(newPerson.getLastName());
                    person.setFirstName(newPerson.getFirstName());
                    person.setBranchId(newPerson.getBranchId());

                    return personRepo.save(person);
                })
                .orElseGet(() -> {
                    newPerson.setPersonId(id);
                    return personRepo.save(newPerson);
                });
    }

    public void deletePerson(Long id) {
        personRepo.deleteById(id);
    }

    /*
    public Object getPersonsBranch(Long id) {
        // return personRepo.findById(id)
        return branchRepo.findByBranchId(personRepo.findById(id).get().getBranchId());
    }
     */
}
