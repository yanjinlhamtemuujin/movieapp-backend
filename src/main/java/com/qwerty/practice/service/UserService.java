package com.qwerty.practice.service;
import com.qwerty.practice.entity.User;
import com.qwerty.practice.exception.UserFoundException;
import com.qwerty.practice.exception.UserNotFoundException;
import com.qwerty.practice.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;


@Service
@CrossOrigin(origins = "http://localhost:4200", allowCredentials = "true")
public class UserService {
    @Autowired
    UserRepo userRepo;

    public Object getAllUser() {
        return userRepo.findAllBy();
    }

//    public Object getOneUser(String userName, String password) {
//    @CrossOrigin(origins = "http://localhost:4200")

    public Object getOneUser(User user) {
        String tmp_userName = user.getUserName();
        String tmp_password = user.getPassword();
        User tmp =  userRepo.findByUserNameAndPassword(tmp_userName, tmp_password);
        if (tmp == null) {
            throw new UserNotFoundException();
        }
        return tmp;
    }

    public Object addUser(User persons) {
        String tmp_email = persons.getEmail();
        String tmp_username = persons.getUserName();
//        if (userRepo.findByEmail(tmp_email) == null && userRepo.findByUserName(tmp_username) == null) {
//            return userRepo.save(persons);
//        }
//        throw new UserFoundException(tmp_email);
        return userRepo.save(persons);

    }

    public Object replaceUser(User newPerson, Long id) {
        return userRepo.findById(id)
                .map(person -> {
                    person.setLastName(newPerson.getLastName());
                    person.setFirstName(newPerson.getFirstName());
                    return userRepo.save(person);
                })
                .orElseGet(() -> {
                    newPerson.setUserId(id);
                    return userRepo.save(newPerson);
                });
    }

    public void deleteUser(Long id) {
        userRepo.deleteById(id);
    }

}
