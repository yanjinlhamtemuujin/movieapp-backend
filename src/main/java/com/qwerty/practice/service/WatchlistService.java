package com.qwerty.practice.service;

import com.qwerty.practice.entity.Watchlist;
import com.qwerty.practice.exception.FoundInWatchlistException;
import com.qwerty.practice.exception.NotFoundInWatchlistException;
import com.qwerty.practice.repository.WatchlistRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Service
@CrossOrigin(origins = "http://localhost:4200", allowCredentials = "true")
public class WatchlistService {

    @Autowired
    WatchlistRepo watchlistRepo;

    public List<Watchlist> getWatchlist(Long userId) {
        List<Watchlist> tmp = watchlistRepo.findByUserId(userId);

        if (tmp == null) {
            throw new NotFoundInWatchlistException(userId);
        }
        return tmp;
    }

    public Object addToWatchlist(Watchlist watchlist) {
        Long tmp_movieId = watchlist.getMovieId();
        Long tmp_userId = watchlist.getUserId();
        if (watchlistRepo.findByMovieIdAndUserId(tmp_movieId, tmp_userId) == null) {
            System.out.println(watchlistRepo.findByMovieIdAndUserId(tmp_movieId, tmp_userId));
            return watchlistRepo.save(watchlist);
        }
        throw new FoundInWatchlistException(tmp_movieId, tmp_userId);
    }

    public void deleteFromWatchlist(Long movieId, Long userId) {
        watchlistRepo.deleteByMovieIdAndUserId(movieId, userId);
    }
}










