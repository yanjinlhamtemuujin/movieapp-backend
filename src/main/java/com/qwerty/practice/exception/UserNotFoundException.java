package com.qwerty.practice.exception;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException() {
        super("Could not find user ");
    }
}

