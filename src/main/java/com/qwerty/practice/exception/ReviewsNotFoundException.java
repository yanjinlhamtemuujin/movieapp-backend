package com.qwerty.practice.exception;

public class ReviewsNotFoundException extends RuntimeException {
    public ReviewsNotFoundException(Long id) {
        super("Could not find reviews of movie " + id);
    }
}
