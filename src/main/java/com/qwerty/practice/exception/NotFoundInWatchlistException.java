package com.qwerty.practice.exception;


public class NotFoundInWatchlistException extends RuntimeException {
    public NotFoundInWatchlistException(Long userId) {
        super("User " + userId + " 's Watchlist is empty");
    }
}
