package com.qwerty.practice.exception;

public class BranchNotFoundException extends RuntimeException {
    public BranchNotFoundException(Long id) {
        super("Could not find branch " + id);
    }
}

