package com.qwerty.practice.entity;

import lombok.Data;
import javax.persistence.*;

@Entity
@Data
@Table(name = "PERSONS")
public class Persons {

    @Id
    // @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PERSONID")
    private Long personId;

    @Column(name = "LASTNAME")
    private String lastName;

    @Column(name = "FIRSTNAME")
    private String firstName;

    @Column(name = "BRANCHID")
    private Long branchId;
}
