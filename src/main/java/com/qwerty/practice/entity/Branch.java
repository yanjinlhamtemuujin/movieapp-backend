package com.qwerty.practice.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "BRANCH")
public class Branch {
    @Id
    @Column(name = "BRANCHID")
    private Long branchId;

    @Column(name = "BRANCHNAME")
    private String branchName;

    @Column(name = "MGRID")
    private Long mgrId;
}
