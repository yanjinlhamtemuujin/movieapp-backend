package com.qwerty.practice.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "REVIEWS")
public class Reviews {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "REVIEWID")
    private Long reviewId;

    @Column(name = "MOVIEID")
    private Long movieId;

    @Column(name = "USERID")
    private Long userId;

    @Column(name = "HEADLINE")
    private String headline;

    @Column(name = "CONTENT")
    private String content;

    @Column(name = "RATING")
    private Double rating;

    @Column(name = "DATE")
    @CreationTimestamp
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Ulaanbaatar")
    private Date date;

    @Transient
    private String userName;

    public Reviews() {
    }

    public Reviews (Reviews reviews, String userName) {
        this.reviewId = reviews.getReviewId();
        this.movieId = reviews.getMovieId();
        this.userId = reviews.getUserId();
        this.headline = reviews.getHeadline();
        this.content = reviews.getContent();
        this.rating = reviews.getRating();

        this.userName = userName;
    }
}
