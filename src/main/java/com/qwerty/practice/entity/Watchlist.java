package com.qwerty.practice.entity;

import lombok.Data;
import javax.persistence.*;

@Entity
@Data
@Table(name = "WATCHLIST")
public class Watchlist {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "USERID")
    private Long userId;

    @Column(name = "MOVIEID")
    private Long movieId;

    @Column(name = "OVERVIEW")
    private String overview;

    @Column(name = "PATH")
    private String path;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "RATING")
    private Double rating;
}
