package com.qwerty.practice.controller;

import com.qwerty.practice.entity.User;
import com.qwerty.practice.exception.UserFoundException;
import com.qwerty.practice.exception.UserNotFoundException;
import com.qwerty.practice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;        

@RestController
@RequestMapping(path="/api")
@CrossOrigin(origins = "http://localhost:4200", allowCredentials = "true")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping(value = "/user")
    public ResponseEntity getAllUser() {
        return ResponseEntity.ok(userService.getAllUser());
    }

//    @GetMapping(value = "/user/{id}")
    @PostMapping(value = "/getOneUser")
    public ResponseEntity getOneUser(@RequestBody User user) throws UserNotFoundException {
        return ResponseEntity.ok(userService.getOneUser(user));
    }

    @PostMapping(value = "/addUser")
    public ResponseEntity addUser(@RequestBody User user) throws UserFoundException {
        return ResponseEntity .ok(userService.addUser(user));
    }

    @PutMapping(value = "/user/{id}")
    public ResponseEntity replaceUser(@RequestBody User newUser, @PathVariable Long id) {
        return ResponseEntity.ok(userService.replaceUser(newUser, id));
    }

    @DeleteMapping(value = "/user/{id}")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }

}
