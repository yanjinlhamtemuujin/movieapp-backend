package com.qwerty.practice.controller;

import com.qwerty.practice.entity.Branch;
import com.qwerty.practice.service.BranchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/api")
public class BranchController {
    @Autowired
    BranchService branchService;

    @GetMapping(value = "/branch")
    public ResponseEntity getAllBranch() {
        return ResponseEntity.ok(branchService.getAllBranch());
    }

    @GetMapping(value = "/branch/{id}")
    public ResponseEntity getOneBranch(@PathVariable Long id) {
        return ResponseEntity.ok(branchService.getOneBranch(id));
    }

    @PostMapping(value = "/addBranch")
    public ResponseEntity addBranch(@RequestBody Branch branch) {
        return ResponseEntity.ok(branchService.addBranch(branch));
    }

    @PutMapping(value = "/branch/{id}")
    public ResponseEntity replaceBranch(@RequestBody Branch newBranch, @PathVariable Long id) {
        return ResponseEntity.ok(branchService.replaceBranch(newBranch, id));
    }

    @DeleteMapping(value = "/branch/{id}")
    public void deleteBranch(@PathVariable Long id) {
        branchService.deleteBranch(id);
    }

}
