package com.qwerty.practice.controller;

import com.qwerty.practice.entity.Reviews;
import com.qwerty.practice.service.ReviewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/api")
@CrossOrigin(origins = "http://localhost:4200", allowCredentials = "true")
public class ReviewsController {
    @Autowired
    ReviewsService reviewsService;

    @GetMapping(value = "/reviews")
    public ResponseEntity getAllReviews() {
        return ResponseEntity.ok(reviewsService.getAllReviews());
    }

    @GetMapping(value = "/reviews/{id}")
    public ResponseEntity getOneReviews(@PathVariable Long id) {
        return ResponseEntity.ok(reviewsService.getOneReviews(id));
    }

    @PostMapping(value = "/addReview")
    public ResponseEntity addReview(@RequestBody Reviews user) {
        return ResponseEntity.ok(reviewsService.addReview(user));
    }

//    @PutMapping(value = "/reviews/{id}")
//    public ResponseEntity replaceReviews(@RequestBody Reviews newReviews, @PathVariable Long id) {
//        return ResponseEntity.ok(reviewsService.replaceReviews(newReviews, id));
//    }

    @DeleteMapping(value = "/reviews/{id}")
    public void deleteReviews(@PathVariable Long id) {
        reviewsService.deleteReviews(id);
    }

    /*
    @GetMapping(value = "/reviews/{id}/branch")
    public ResponseEntity getReviewsBranch(@PathVariable Long id) {
        return ResponseEntity.ok(personService.getReviewssBranch(id));
    }
     */
}
