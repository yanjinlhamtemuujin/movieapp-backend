package com.qwerty.practice.controller;

import com.qwerty.practice.entity.Watchlist;
import com.qwerty.practice.exception.FoundInWatchlistException;
import com.qwerty.practice.exception.NotFoundInWatchlistException;
import com.qwerty.practice.service.WatchlistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(path="/api")
@CrossOrigin(origins = "http://localhost:4200", allowCredentials = "true")
public class WatchlistController {
    @Autowired
    WatchlistService watchlistService;

    @GetMapping(value = "/watchlist/{userId}")
    public ResponseEntity getWatchlist(@PathVariable Long userId) throws NotFoundInWatchlistException {
        return ResponseEntity.ok(watchlistService.getWatchlist(userId));
    }

    @PostMapping(value = "/addToWatchlist")
    public ResponseEntity addToWatchlist(@RequestBody Watchlist watchlist) throws FoundInWatchlistException {
        return ResponseEntity.ok(watchlistService.addToWatchlist(watchlist));
    }

    @DeleteMapping(value = "/deleteFromWatchlist/{userId}/{movieId}")
    public void deleteFromWatchlist(@PathVariable Long userId, @PathVariable Long movieId) {
        watchlistService.deleteFromWatchlist(movieId, userId);
    }
}
