package com.qwerty.practice.controller;

import com.qwerty.practice.entity.Persons;
import com.qwerty.practice.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/api")
public class PersonController {
    @Autowired
    PersonService personService;

    @GetMapping(value = "/persons")
    public ResponseEntity getAllPerson() {
        return ResponseEntity.ok(personService.getAllPerson());
    }

    @GetMapping(value = "/persons/{id}")
    public ResponseEntity getOnePerson(@PathVariable Long id) {
        return ResponseEntity.ok(personService.getOnePerson(id));
    }

    @PostMapping(value = "/addPerson")
    public ResponseEntity addPerson(@RequestBody Persons persons) {
        return ResponseEntity.ok(personService.addPerson(persons));
    }

    @PutMapping(value = "/persons/{id}")
    public ResponseEntity replacePerson(@RequestBody Persons newPerson, @PathVariable Long id) {
        return ResponseEntity.ok(personService.replacePerson(newPerson, id));
    }

    @DeleteMapping(value = "/persons/{id}")
    public void deletePerson(@PathVariable Long id) {
        personService.deletePerson(id);
    }

    /*
    @GetMapping(value = "/persons/{id}/branch")
    public ResponseEntity getPersonBranch(@PathVariable Long id) {
        return ResponseEntity.ok(personService.getPersonsBranch(id));
    }
     */
}
